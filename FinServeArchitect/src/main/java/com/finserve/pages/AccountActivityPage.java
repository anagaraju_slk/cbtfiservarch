package com.finserve.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.finserve.utilities.Report;

public class AccountActivityPage extends BasePage{

	public AccountActivityPage(WebDriver driver) {
		super(driver);
		
	}
	
	@FindBy(xpath="//div[@id='M_Menu_div']//div[text()='Accounts']")
	private WebElement accountMenu;

	@FindBy(xpath="//div[@id='M_Menu_div']//div[text()='Account Activity']")
	private WebElement accounActivity;
	
	@FindBy (id="M_layout_content_PCDZ_M2SN6LC_ctl00_transactionTypes-button")
	private WebElement showAllDropdown;
	
	@FindBy (xpath="//a[contains(text(),'Checks')]")
	private WebElement checksOption;
	
	@FindBy (xpath="//a[contains(text(),'Deposits')]")
	private WebElement depositOption;
	
	@FindBy (id="M_layout_content_PCDZ_M2SN6LC_ctl00_description")
	private WebElement descriptionTextbox;
	
	@FindBy (id="M_layout_content_PCDZ_M2SN6LC_ctl00_search")
	private WebElement searchButton;
	
	@FindBy (xpath="//a[contains(text(), 'Regular')]")
	private WebElement checkingAccount;
	
	@FindBy (id="M_layout_content_PCDZ_M2SN6LC_ctl00_selectedAccount-button")
	private WebElement selectIcon;
	
	@FindBy (xpath="//label[contains(text(), 'All')]")
	private WebElement allRadiobutton; 
	
	@FindBy(xpath="//*[text()=*]")
	private List<WebElement> allSubMenuText;
	
	@FindBy(xpath="//div[@data-moduletitle='Account Details']")
	private List<WebElement> accountDetails;
	
	@FindBy(xpath="//th[contains(.,'Account Nickname')]/..//span")
	private WebElement accountNickName;
	
	@FindBy(xpath="//tr[@class='row_alternating']/..//td[contains(text(), \"Lifetime Free \")]")
	private WebElement accountName;
	
	@FindBy(xpath="//tr/td[contains(text(), \"x2106\")]")
	private WebElement accountNumber;
	
	@FindBy(xpath="//tr/td[contains(text(), \"05/17/2017\")]")
	private WebElement dateOpened;
	
	@FindBy(xpath="//ul[contains(@id, 'transactionTypes-menu')]/li")
	private List<WebElement> transactionTypes;
	
	
	public void clickOnAccountActivitySubMenu() {
		accountMenu.click();
		waitForElementVisible(accounActivity);
		accounActivity.click();
	}
	
	public void selectCheckingAccount() {
		selectIcon.click();
		waitForElementVisible(checkingAccount);
		checkingAccount.click();
	}
	
	public void clickShowOnlyDropdown() {
		waitForElementVisible(showAllDropdown);
		showAllDropdown.click();
	}
	
	public void selectChecksOption() {
		waitForElementVisible(checksOption);
		checksOption.click();
	}
	
	public void selectDepositOption() {
		waitForElementVisible(depositOption);
		depositOption.click();
	}
	
	public boolean verifyAccountNickName() {
		boolean isEmpty = false;
		if(!accountNickName.getText().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}
	

	
	public boolean verifyAccountName() {
		boolean isEmpty = false;
		if(!accountName.getText().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}
	
	public boolean verifyDateOpened() {
		boolean isEmpty = false;
		if(!dateOpened.getText().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}
	
	public boolean verifyAccountNumber() {
		boolean isEmpty = false;
		if(!accountNumber.getText().isEmpty()) {
			isEmpty = true;
		}
		return isEmpty;
	}
		
	public void enterDescrition() {
		waitForElementVisible(descriptionTextbox);
		descriptionTextbox.sendKeys("Deposit (340) Test");
	}
	
	public void clickOnSearchButton() {
		waitForElementVisible(searchButton);
		searchButton.click();
	}
	
	public List<String> getTransactionTypes(){
		ArrayList<String> types = new ArrayList<String>();
		for(WebElement type : transactionTypes) {
			String str = type.getText();
			types.add(str);
		}
		return types;
	}
	public void selectAllRadioButton() {
		JavascriptExecutor JS = (JavascriptExecutor)driver;
		JS.executeScript("arguments[0].click()",allRadiobutton);
	}

	public void verifyAccountActivityTitle(List<String> Titles) {
		for (WebElement submenu : allSubMenuText) {
			for (String title : Titles) {
				if(submenu.getText().contains(title)) {
					Report.log("pass", " Title is present: "+title);
					break;
				}
			}
		}
		
		
    }
	
	public void verifyAccountDetails(String accountNickName, String accountName, String acountNumber, String dateOpened ) {
		if(accountName.equals(accountName)) {
			
		}
	}
}