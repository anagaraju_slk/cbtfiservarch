package com.finserve.runner;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.finserve.utilities.Generic;
import com.finserve.utilities.IConstants;
import com.finserve.utilities.Report;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

/**
 * Class to start the execution.
 */
@CucumberOptions(
		features = {"features"},
		glue = {"com.finserve.steps"}, 
		tags= {"@AccountActivity1"})
public class TestRunner extends AbstractTestNGCucumberTests implements IConstants
{
	@BeforeSuite
	public void suiteSetUp() {
		String htmlPath = HTMLPATH + "Test_Report_" + Generic.getCurrentDate() + ".html";
		Report.configHTML(htmlPath);
	}

	@AfterSuite
	public void suiteTearDown() {
		Report.flush();
		Generic.deleteTempDir();
	}
}
