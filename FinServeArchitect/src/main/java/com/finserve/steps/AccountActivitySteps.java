package com.finserve.steps;

import java.util.List;

import org.testng.Assert;

import com.finserve.pages.AccountActivityPage;
import com.finserve.utilities.Excel;
import com.finserve.utilities.Generic;
import com.finserve.utilities.PageManager;
import com.finserve.utilities.Report;
import com.finserve.utilities.WebDriverManager;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class AccountActivitySteps {
	AccountActivityPage accountActivity;
	Excel excel;

	public AccountActivitySteps() {
		accountActivity = PageManager.getAccountActivityPage(WebDriverManager.driver);
	}

	@Given("^Click on AccountsActivity$")
	public void click_on_AccountsActivity() throws Throwable {
		accountActivity.clickOnAccountActivitySubMenu();

	}

	@Then("^Verify accountActivitypage \"([^\"]*)\"$")
	public void verify_accountActivitypage(String title) throws Throwable {
		boolean isPresent = accountActivity.verifyTitle(title);
		Assert.assertEquals(isPresent, true);

	}

	@Then("^select checking account$")
	public void select_checking_account() throws Throwable {
		accountActivity.selectCheckingAccount();

	}

	@Then("^click on Show only dropdown$")
	public void click_on_Show_only_dropdown() throws Throwable {
		accountActivity.clickShowOnlyDropdown();

	}

	@Then("^select chekcs option$")
	public void select_chekcs_option() throws Throwable {
		accountActivity.selectChecksOption();

	}

	@Then("^enter \"([^\"]*)\" in the description text box$")
	public void enter_in_the_description_text_box(String arg1) throws Throwable {
		accountActivity.enterDescrition();

	}

	@Then("^click on search button$")
	public void click_on_search_button() throws Throwable {
		accountActivity.clickOnSearchButton();

	}

	@Then("^Select Checking account in Account History dropdown$")
	public void select_Checking_account_in_Account_History_dropdown() throws Throwable {
		accountActivity.selectCheckingAccount();

	}

	@Then("^click on All radio button$")
	public void click_on_All_radio_button() throws Throwable {
		accountActivity.selectAllRadioButton();

	}

	@Then("^select Deposit option$")
	public void select_Deposit_option() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

	@Then("^verify Account Activity Page \"([^\"]*)\"$")
	public void verify_Account_Activity_Page(String title) throws Throwable {
		boolean isPresent = accountActivity.verifyTitle(title);
		Assert.assertEquals(isPresent, true);
	}

	@Then("^verify Account details tail$")
	public void verify_Account_details_tail() {
		Report.log("info", "Verifying the Account Nickname  element in Account Details section");
		Assert.assertEquals(accountActivity.verifyAccountName(), true);
		
		Report.log("info",  "Verifying the Account Name element in Account Details section");
		Assert.assertEquals(accountActivity.verifyAccountName(), true);
		
		Report.log("info",  "Verifying the Account Number element in Account Details section");
		Assert.assertEquals(accountActivity.verifyAccountNumber(), true);
		
		Report.log("info",  "Verifying the Date opened element in Account Details section");
		Assert.assertEquals(accountActivity.verifyAccountName(), true);
	}

	@Then("^verify dropdown values$")
	public void verify_dropdown_values() {
		int rows = Excel.getTotalRow(WebDriverManager.EXCEL_PATH, "AcountActivity");
		System.out.println(rows);
		int column = 0;
		for (int i = 1; i <= rows; i++) {
			String trancationType = Excel.getValue(WebDriverManager.EXCEL_PATH, "AcountActivity", i, column);
			List<String> types = accountActivity.getTransactionTypes();
			boolean status = Generic.verifyText(types, trancationType);
			if (status) {
				Report.log("pass", "Transaction type: " + trancationType + " found in acount activity page");
				Assert.assertEquals(status, true);
			} else {
				Report.log("fail", "Transaction type: " + trancationType + " not found in acount activity page");
				Assert.assertEquals(status, true);
			}
		}
	}

	}
