@Accounts
Feature: Account

  @AccountsSummary
  Scenario Outline: TC_001 Account Summary
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    And Click on accounts
    Then Verify account summary page "<Title>"
    Then Verify Titles "Key Expenses" "Quick Transfers" "Portfolio" "Top Expenses" "Recent Transactions" "Loans" "Deposits"

    Examples: 
      | UserName | Password   | Title                                  |
      | slktest3 | Testuser@3 | Accounts Summary - Carter Bank & Trust |

      
      @AccountsSummary
  Scenario Outline: TC_001 Account Summary
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    And Click on accounts
    Then Verify account summary page "<Title>"
    Then Verify Titles "Key Expenses" "Quick Transfers" "Portfolio" "Top Expenses" "Recent Transactions" "Loans" "Deposits"
    

    Examples: 
      | UserName | Password   | Title                                  |
      | slktest3 | Testuser@3 | Accounts Summary - Carter Bank & Trust |
      