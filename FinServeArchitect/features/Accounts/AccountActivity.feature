@Accounts
Feature: AccountActivity

  @AccountActivity1
  Scenario Outline: TC_001 Account Details - Checking
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    Then Click on AccountsActivity
    Then verify Account Activity Page "<Title>"
    Then select checking account
    Then click on search button
    Then verify Account details tail 

    Examples: 
      | UserName | Password   | Title                                  | description        |
      | slktest1 | Testuser@1 | Account Activity - Carter Bank & Trust | Deposit (340) Test |

  @AccountActivityVerification
  Scenario Outline: TC_023 Account_History_Checks_60_days_Description
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    Then Click on AccountsActivity
    And Verify accountActivitypage "<Title>"
    Then select checking account
    Then click on Show only dropdown
    Then verify dropdown values
    Then select chekcs option
    Then enter "<description>" in the description text box
    Then click on search button

    Examples: 
      | UserName | Password   | Title                                  | description        |
      | slktest1 | Testuser@1 | Account Activity - Carter Bank & Trust | Deposit (340) Test |

  @AccountActivity
  Scenario Outline: TC_024 Account - History_Checks - 60 days
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    Then Click on AccountsActivity
    Then select checking account
    Then click on Show only dropdown
    Then select chekcs option
    Then click on search button

    Examples: 
      | UserName | Password   | Title                                  | description        |
      | slktest1 | Testuser@1 | Account Activity - Carter Bank & Trust | Deposit (340) Test |

  @AccountActivity
  Scenario Outline: TC_025 Account - History_Deposits - All (Amount)
    Given Login to Architect with a valid Userid/Pwd "<UserName>" and "<Password>"
    Then Click on AccountsActivity
    Then select checking account
    Then click on All radio button
    Then click on Show only dropdown
    Then select Deposit option
    Then click on search button

    Examples: 
      | UserName | Password   | Title                                  | description        |
      | slktest1 | Testuser@1 | Account Activity - Carter Bank & Trust | Deposit (340) Test |
