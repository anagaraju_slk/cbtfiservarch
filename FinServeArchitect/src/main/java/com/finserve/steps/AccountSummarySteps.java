package com.finserve.steps;

import java.util.ArrayList;

import org.testng.Assert;

import com.finserve.pages.AccountSummaryPage;
import com.finserve.utilities.PageManager;
import com.finserve.utilities.WebDriverManager;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class AccountSummarySteps {
	AccountSummaryPage accountSummary;
	
	public AccountSummarySteps() {
		accountSummary = PageManager.getAccountSummaryPage(WebDriverManager.driver);
	}

	
	@Given("^Click on accounts$")
	public void click_on_accounts() throws Throwable {
		accountSummary.clickOnAccountMenu();
	}

	@Then("^Verify account summary page \"([^\\\"]*)\"$")
	public void verify_account_summary_page(String title) throws Throwable {
		boolean isPresent = accountSummary.verifyTitle(title);
		Assert.assertEquals(isPresent, true);
	}

	@Then("^Verify Titles \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_Titles(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, String arg7) throws Throwable {
		ArrayList<String> title = new ArrayList<String>();
		title.add(arg1);title.add(arg2);title.add(arg3);title.add(arg4);title.add(arg5);title.add(arg6);title.add(arg7);
		accountSummary.verifyAccountSummaryTitle(title);
	}
}
