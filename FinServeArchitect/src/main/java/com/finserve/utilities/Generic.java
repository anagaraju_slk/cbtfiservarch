package com.finserve.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class Generic {
	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss");
		return sf.format(date.getTime());
	}

	public static void takeScreenShot(WebDriver driver, String screenShotPath) {
		TakesScreenshot screen = (TakesScreenshot) driver;
		File src = screen.getScreenshotAs(OutputType.FILE);
		File dest = new File(screenShotPath);
		try {
			Files.copy(src, dest);
		} catch (IOException e) {
			Report.log("fail", "Unable to take the screen shot: " + e);
		}
	}

	public static void deleteTempDir() {
		try {
			File temp = File.createTempFile("temp-file-name", ".tmp");
			System.out.println("Temp file : " + temp.getAbsolutePath());
			String absolutePath = temp.getAbsolutePath();
			String tempFilePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
			System.out.println("Temp file path : " + tempFilePath);
			File f = new File(tempFilePath);
			f.delete();
		} catch (Exception exp) {
			System.out.println(exp);
		}
	}
	public static boolean verifyText(List<String> list, String text) {
		boolean found = false;
		for (String l : list) {
			if (l.contains(text)) {
				found = true;
				break;
			}
		}
		return found;
	}


}
