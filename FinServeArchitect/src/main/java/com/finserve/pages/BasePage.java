package com.finserve.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	WebDriver driver;
	private WebDriverWait wait;

	public BasePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		this.driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}

	public Boolean verifyTitle(String title) {
		wait = new WebDriverWait(driver, 15);
		Boolean isTitle = false;
		try {
			isTitle = wait.until(ExpectedConditions.titleIs(title));
		} catch (TimeoutException e) {
			System.out.println("fail" + title + " is not matching");
		}
		return isTitle;
	}

	public void waitForElementVisible(WebElement element) {
		wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException e) {
			System.out.println("fail" + element.toString() + " is not visible");
		}
	}
}
