package com.finserve.utilities;

import org.openqa.selenium.WebDriver;

import com.finserve.pages.AccountActivityPage;
import com.finserve.pages.AccountSummaryPage;
import com.finserve.pages.LoginPage;

public class PageManager {
	
	public static LoginPage getLoginPage(WebDriver driver) {
		return new LoginPage(driver);
	}
	
	public static AccountSummaryPage getAccountSummaryPage(WebDriver driver) {
		return new AccountSummaryPage(driver);
	}
	
	public static AccountActivityPage getAccountActivityPage(WebDriver driver) {
		return new AccountActivityPage(driver);
	}
}
