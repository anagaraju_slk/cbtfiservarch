package com.finserve.utilities;

public interface IConstants {
	
	String CHROME_KEY = "webdriver.chrome.driver";
	String CHROME_PATH = "./src/test/resources/drivers/chromedriver.exe";
	
	String FIREFOX_KEY = "webdriver.gecko.driver";
	String FIREFOX_PATH = "./src/test/resources/drivers/geckodriver.exe";
	
	String EDGE_KEY = "webdriver.chrome.driver";
	String EDGE_PATH = "./src/test/resources/drivers/IEDriverServer.exe";
	
	String IE_KEY = "webdriver.chrome.driver";
	String IE_PATH = "./src/test/resources/drivers/MicrosoftWebDriver.exe";
	
	String HTMLPATH = "./Reports/TestResults/";
	String SCREENSHOTPATH = "./Reports/screenshots/";
	
	String EXCEL_KEY = "TestData.xlsx";
	String EXCEL_PATH = "./src/test/resources/configarations/TestData.xlsx";
}
