package com.finserve.steps;

import com.finserve.pages.LoginPage;
import com.finserve.utilities.PageManager;
import com.finserve.utilities.WebDriverManager;
import cucumber.api.java.en.Given;

public class LoginSteps {
	LoginPage loginPage;

	public LoginSteps() {
		loginPage = PageManager.getLoginPage(WebDriverManager.driver);
	}

	@Given("^Login to Architect with a valid Userid/Pwd \"([^\"]*)\" and \"([^\"]*)\"$")
	public void login_to_Architect_with_a_valid_Userid_Pwd_and(String username, String password) throws Throwable {
	    loginPage.login(username, password);
	}
}
