package com.finserve.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	private Properties properties;
	private final String propertyFilePath= "src//test//resources//configarations//config.properties";
 
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Report.log("Exception", "Configuration.properties not found at: " + propertyFilePath);
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
 
	public long getImplicitlyWait() {		
		String implicitlyWait = properties.getProperty("implicitlyWait");
			try{
				return Long.parseLong(implicitlyWait);
			}catch(NumberFormatException e) {
				Report.log("Exception", "Not able to parse value : : " + implicitlyWait + " in to Long");
				throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
			}
	}
 
	public String getApplicationUrl() {
		String url = properties.getProperty("url");
		if(url != null) return url;
		else throw new RuntimeException("Application Url not specified in the Configuration.properties file for the Key:url");
	}
 
}
