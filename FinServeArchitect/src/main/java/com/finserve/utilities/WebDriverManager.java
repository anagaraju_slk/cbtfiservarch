package com.finserve.utilities;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class WebDriverManager implements IConstants {
	public static WebDriver driver;
	public static EventFiringWebDriver edriver;

	public static void createDriver(String browserName) {
		try {
			switch (browserName.toUpperCase()) {
			case "FIREFOX":
				System.setProperty(FIREFOX_KEY, FIREFOX_PATH);
				driver = new FirefoxDriver();
				driver.manage().window().maximize();
				break;
			case "CHROME":
				System.setProperty(CHROME_KEY, CHROME_PATH);
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				options.addArguments("--disable-infobars");
				driver = new ChromeDriver(options);
				break;
			case "INTERNETEXPLORER":
				System.setProperty(IE_KEY, IE_PATH);
				driver = new InternetExplorerDriver();
				break;
			case "EDGE":
				System.setProperty(EDGE_KEY, EDGE_PATH);
				driver = new EdgeDriver();
			default:
				System.out.println("warning: pass valid browser name" + browserName);
				break;
			}
			//EventFiringWebDriver driver = new EventFiringWebDriver(edriver);
			//Listeners eventListener = new Listeners();
			//driver.register(eventListener);
			driver.navigate().to("https://carterbank.architect-cert.fiservapps.com/SignIn.aspx");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception exception) {
			Report.log("Fail", "Unable to launch " + browserName + "\n" + exception);
		}
	}

	public static void closeDriver() {
		driver.quit();
	}

}
