package com.finserve.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BasePage{
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath="//label[text()='Username']/..//input")
	private WebElement usernameTB;
	
	@FindBy(xpath="//label[text()='Password']/..//input")
	private WebElement passwordTB;
	
	@FindBy(how = How.XPATH, using = "//input[@value='SIGN IN']")
	private WebElement loginButton;
	
	public void login(String username, String password)
	{
		usernameTB.sendKeys(username);
		passwordTB.sendKeys(password);
		loginButton.click();
	}
	
}
