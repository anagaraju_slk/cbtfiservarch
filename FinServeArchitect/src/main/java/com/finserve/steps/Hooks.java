package com.finserve.steps;

import com.finserve.utilities.Generic;
import com.finserve.utilities.IConstants;
import com.finserve.utilities.Report;
import com.finserve.utilities.WebDriverManager;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks implements IConstants{
	
	@Before
	public void startTest(Scenario scenario) {
		Report.createTest(scenario.getName());
		Report.log("info", scenario.getName() + " Execution started");
		WebDriverManager.createDriver("Firefox");
	}

	@After
	public void endTest(Scenario scenario) {
		if (scenario.isFailed()) {
			Report.log("fail", scenario.getName()+" Failed");
			String screenShotPath = SCREENSHOTPATH + scenario.getName() + "_" + System.currentTimeMillis() + ".png";
			Generic.takeScreenShot(WebDriverManager.driver, screenShotPath);
			Report.addScreenShotToReport(System.getProperty("user.dir")+"/"+screenShotPath);
		}
		WebDriverManager.closeDriver();
	}
}
