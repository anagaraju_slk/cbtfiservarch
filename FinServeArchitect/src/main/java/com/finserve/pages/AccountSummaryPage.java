package com.finserve.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.finserve.utilities.Report;

public class AccountSummaryPage extends BasePage{
	public AccountSummaryPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath="//div[@id='M_Menu_div']//div[text()='Accounts']")
	private WebElement accountMenu;
	
	@FindBy(xpath="//ul[@id='M_Menu']//div[text()='Accounts Summary']")
	private WebElement accountSummarySubMenu;
	
	@FindBy(xpath="//*[text()=*]")
	private List<WebElement> allSubMenuText;
	
	public void clickOnAccountMenu() {
		accountMenu.click();
		waitForElementVisible(accountSummarySubMenu);
		accountSummarySubMenu.click();
	}
	
	public void verifyAccountSummaryTitle(List<String> Titles) {
		for (WebElement submenu : allSubMenuText) {
			for (String title : Titles) {
				if(submenu.getText().contains(title)) {
					Report.log("pass", " Title is present: "+title);
					break;
				}
			}
		}
	}
}
