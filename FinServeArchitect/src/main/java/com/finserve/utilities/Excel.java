package com.finserve.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Excel {
	public static String getValue(String excelPath, String sheetName, int rowNum, int cellNum) {
		String value;
		try {
			FileInputStream fis = new FileInputStream(excelPath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sheet = wb.getSheet(sheetName);
			value = sheet.getRow(rowNum).getCell(cellNum).getStringCellValue();
		} catch (Exception e) {
			value = "";
		}
		return value;
	}

	public static int getTotalRow(String excelPath, String sheetName) {
		int rowCount;
		try {
			FileInputStream fis = new FileInputStream(excelPath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sheet = wb.getSheet(sheetName);
			rowCount = sheet.getLastRowNum();
		} catch (Exception e) {
			System.out.println(e);
			rowCount = -1;
		}
		return rowCount;
	}

	public static void setValueToCell(String excelPath, String sheetName, int rowNum, int cellNum,
			String valueToWrite) {
		try {
			FileInputStream fis = new FileInputStream(excelPath);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sheet = wb.getSheet(sheetName);
			sheet.getRow(rowNum).getCell(cellNum).setCellValue(valueToWrite);
		} catch (Exception e) {
			System.out.println("Unable to set the value");
		}
	}

	public static void writeToExcel(String excelPath) {
		try {
			FileInputStream fis = new FileInputStream(excelPath);
			Workbook wb = WorkbookFactory.create(fis);
			FileOutputStream fos = new FileOutputStream(excelPath);
			wb.write(fos);
		} catch (Exception e) {
			System.out.println("Unable to write");
		}
	}

}
